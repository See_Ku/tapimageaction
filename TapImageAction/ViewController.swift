//
//  ViewController.swift
//  TapImageAction
//
//  Created by See.Ku on 2016/07/02.
//  Copyright (c) 2016 AxeRoad. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var imageView: UIImageView!

	@IBAction func onTapImage(sender: AnyObject) {

		// セグエを使用して画面を遷移
		performSegueWithIdentifier("result", sender: nil)
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		let image = UIImage(named: "fruit_slice11_kiwi")
		imageView.image = image
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}


}

